# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntellJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download


## Java JDK
The latest version of Java JDK installed. 
* [Java JDK Download](https://adoptopenjdk.net/)
  
*N.B.* This is not the ordinary Oracle download above, because starting with version 11, Oracle plan to charge for support of Java, which means you are better off going with OpenJDK in the first place. Go for the latest version and the HotSpot engine.

* OpenJDK 15
* HotSpot

[Direct Link to Windows Installer](https://github.com/AdoptOpenJDK/openjdk15-binaries/releases/download/jdk-15.0.1%2B9/OpenJDK15U-jdk_x64_windows_hotspot_15.0.1_9.msi)


