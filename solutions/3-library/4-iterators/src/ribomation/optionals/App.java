package ribomation.optionals;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    public void run() {
        final var DEFAULT = "Ooops";
        var strings = new OptionalArrayList<String>();
        System.out.printf("empty: %s%n", strings.lookup(42).orElse(DEFAULT));

        strings.addAll(Arrays.asList("one", "two", "three"));
        System.out.printf("strings[0]: %s%n", strings.lookup(0).orElse(DEFAULT));
        System.out.printf("strings[1]: %s%n", strings.lookup(1).orElse(DEFAULT));
        System.out.printf("strings[2]: %s%n", strings.lookup(2).orElse(DEFAULT));
        System.out.printf("strings[3]: %s%n", strings.lookup(3).orElse(DEFAULT));

        try {
            System.out.println(strings.get(3));
        } catch (Exception e) {
            System.out.println("ERR: " + e);
        }
    }
}
