package ribomation.products;

import java.util.Comparator;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    void run() {
        var products = new ProductGenerator().nextProductList(1000);

        products.sort(Comparator.comparingDouble(Product::getPrice).reversed());
        products.removeIf(p -> p.getCount() != 3);
        products.removeIf(p -> !between.test(800D, p.getPrice(), 900D));

        products.forEach(System.out::println);
    }

    interface TriPredicate<T> {
        boolean test(T lb, T x, T ub);
    }

    TriPredicate<Double> between = (lb, x, ub) -> (lb <= x) && (x <= ub);
}
