package ribomation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
        var products = app.load(Path.of("./data/products.csv"));
//        products.forEach(System.out::println);

        var dir = Files.createTempDirectory("products");
        var file = app.store(dir, products);
        app.size(file);

        var fileSer = app.serialize(dir, products);
        app.size(fileSer);

        var fileGzip = app.gzip(dir, products);
        app.size(fileGzip);

        app.copy(file);
        app.rename(file);
        app.clean(file.getParent());
    }

    List<Product> load(Path path) throws IOException {
        var in = Files.newBufferedReader(path);
        try (in) {
            var products = new ArrayList<Product>();
            var firstLine = true;
            for (var csv = in.readLine(); csv != null; csv = in.readLine()) {
                if (firstLine) {
                    firstLine = false;
                    continue;
                }
                products.add(Product.fromCSV(csv, ";"));
            }
            System.out.printf("loaded %d products%n", products.size());
            return products;
        }
    }

    Path store(Path dir, List<Product> products) throws IOException {
        var file = dir.resolve("products.txt");
        Files.writeString(file, products.toString());
        System.out.printf("written file %s%n", file);
        return file;
    }

    Path serialize(Path dir, List<Product> products) throws IOException {
        var file = dir.resolve("products.ser");

        var os = Files.newOutputStream(file);
        var oos = new ObjectOutputStream(os);
        try (oos) {
            oos.writeObject(products);
        }

        var is = Files.newInputStream(file);
        var ois = new ObjectInputStream(is);
        try (ois) {
            var obj = ois.readObject();
            var eq = obj.equals(products);
            if (!eq) {
                throw new IllegalArgumentException("restored list was not equals to original list");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return file;
    }

    Path gzip(Path dir, List<Product> products) throws IOException {
        var file = dir.resolve("products.ser.gz");

        var os = Files.newOutputStream(file);
        var ogz = new GZIPOutputStream(os);
        var oos = new ObjectOutputStream(ogz);
        try (oos) {
            oos.writeObject(products);
        }

        var is = Files.newInputStream(file);
        var igz = new GZIPInputStream(is);
        var ois = new ObjectInputStream(igz);
        try (ois) {
            var obj = ois.readObject();
            var eq = obj.equals(products);
            if (!eq) {
                throw new IllegalArgumentException("restored list was not equals to original list");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return file;
    }

    void size(Path file) throws IOException {
        if (Files.exists(file)) {
            System.out.printf(Locale.ENGLISH, "%s: %.1f KBytes%n",
                    file, Files.size(file) / 1024.0);
        }
    }

    void copy(Path file) throws IOException {
        var dst = file.getParent().resolve("copy-products.txt");
        Files.copy(file, dst);
        System.out.printf("written file %s%n", dst);
    }

    void rename(Path file) throws IOException {
        var dst = file.getParent().resolve("renamed-products.txt");
        Files.move(file, dst);
        System.out.printf("renamed file %s%n", dst);
    }

    void clean(Path dir) throws IOException {
        Files.list(dir).forEach(p -> {
            try {
                System.out.printf("deleting file %s%n", p);
                Files.delete(p);
            } catch (Exception ignore) {
            }
        });
        Files.delete(dir);
        System.out.printf("deleted dir %s%n", dir);
    }
}
