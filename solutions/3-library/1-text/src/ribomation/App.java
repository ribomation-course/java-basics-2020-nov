package ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        for (String email : testData) {
            System.out.printf("'%s' --> %b%n", email, Email.valid(email));
        }
    }

    final String[] testData = {
            "",
            "   ",
            "jens.riboe.ribomation.se",
            "jens@riboe@ribomation.se",
            "42.riboe@ribomation.se",
            "jens.@ribomation.se",
            "jens.riboe@ribomation.seee",
            "jens.riboe@42ribomation.se",
            "jens.rib#e@ribomation.se",
            "jens.riboe@ribomation.se.",
            "jens.riboe@ribomation.se",
    };
}
