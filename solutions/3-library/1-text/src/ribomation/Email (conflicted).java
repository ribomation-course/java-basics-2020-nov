package ribomation;

public class Email {
    public static boolean valid(String email) {
        if (email == null || email.trim().length() == 0) return false;

        var fields = email.split("@");
        if (fields.length != 2) return false;

        return validName(fields[0]) && validDomain(fields[1]);
    }

    private static boolean validName(String name) {
        if (!name.matches("^[a-zA-Z0-9.-]+$")) return false;
        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.charAt(name.length()-1) == '.') return false;
        return true;
    }

    private static boolean validDomain(String domain) {
        if (!domain.matches("^[a-zA-Z.]+$")) return false;
        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.charAt(domain.length()-1) == '.') return false;

        var tld = domain.substring(domain.lastIndexOf('.') + 1);
        return tld.length() == 2 || tld.length() == 3;
    }
}
