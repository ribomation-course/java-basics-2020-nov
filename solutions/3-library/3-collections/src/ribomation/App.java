package ribomation;

import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        var repo = new ProductRepo();
        var list = repo.create(10);

        System.out.println("--- by insertion order ---");
        list.forEach(System.out::println);

        {
            System.out.println("--- sorted by price desc ---");
//            list.sort((lhs, rhs) -> {
//                var leftPrice = lhs.getPrice();
//                var rightPrice = rhs.getPrice();
//                if (leftPrice < rightPrice) return +1;
//                if (leftPrice > rightPrice) return -1;
//                return 0;
//            });
            list.sort((lhs, rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice()));
            list.forEach(System.out::println);
        }
        {
            System.out.println("--- hash-set (unordered) ---");
            var hashSet = new HashSet<>(list);
            hashSet.forEach(System.out::println);
        }
        {
            System.out.println("--- tree-set by name (duplicate names removed) ---");
            var treeSet = new TreeSet<>(list);
            treeSet.forEach(System.out::println);
        }
        {
            System.out.println("--- tree-set by price desc ---");
            var treeSet = new TreeSet<>((Product lhs, Product  rhs) -> Float.compare(rhs.getPrice(), lhs.getPrice()));
            treeSet.addAll(list);
            treeSet.forEach(System.out::println);
        }
        {
            System.out.println("--- tree-map by name (duplicate names removed) ---");
            var map = new TreeMap<String, Product>();
            list.forEach(p -> map.put(p.getName(), p));
            map.forEach((name, product) -> System.out.printf("%s: %s%n", name, product));
        }
        {
            System.out.println("--- removed even count ---");
            list.removeIf(p -> p.getCount() % 2 == 0);
            list.forEach(System.out::println);
        }
    }
}
