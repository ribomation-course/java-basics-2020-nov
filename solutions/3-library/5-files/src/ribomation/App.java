package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
        var products = app.load(Path.of("./data/products.csv"));
//        products.forEach(System.out::println);
        var file = app.store(products);
        app.size(file);
        app.copy(file);
        app.rename(file);
        app.clean(file.getParent());
    }

    List<Product> load(Path path) throws IOException {
        var in = Files.newBufferedReader(path);
        try (in) {
            var products = new ArrayList<Product>();
            var firstLine = true;
            for (var csv = in.readLine(); csv != null; csv = in.readLine()) {
                if (firstLine) {
                    firstLine = false;
                    continue;
                }
                products.add(Product.fromCSV(csv, ";"));
            }
            System.out.printf("loaded %d products%n", products.size());
            return products;
        }
    }

    Path store(List<Product> products) throws IOException {
        var dir = Files.createTempDirectory("products");
        var outPath = Path.of(dir.toString(), "products.txt");
        Files.writeString(outPath, products.toString());
        System.out.printf("written file %s%n", outPath);
        return outPath;
    }

    void size(Path file) throws IOException {
        System.out.printf(Locale.ENGLISH, "%s: %.1f KBytes%n",
                file, Files.size(file) / 1024.0);
    }

    void copy(Path file) throws IOException {
        var dst = file.getParent().resolve("copy-products.txt");
        Files.copy(file, dst);
        System.out.printf("written file %s%n", dst);
    }

    void rename(Path file) throws IOException {
        var dst = file.getParent().resolve("renamed-products.txt");
        Files.move(file, dst);
        System.out.printf("renamed file %s%n", dst);
    }

    void clean(Path dir) throws IOException {
        Files.list(dir).forEach(p -> {
            try {
                System.out.printf("deleting file %s%n", p);
                Files.delete(p);
            } catch (Exception ignore) { }
        });
        Files.delete(dir);
        System.out.printf("deleted dir %s%n", dir);
    }
}
