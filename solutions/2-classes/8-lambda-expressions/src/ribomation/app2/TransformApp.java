package ribomation.app2;

import java.util.Arrays;

public class TransformApp {
    public static void main(String[] args) {
        var app = new TransformApp();
        app.run();
    }

    void run() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        {
            var result = transform(numbers, n -> n * n);
            System.out.println("result = " + Arrays.toString(result));
        }
        {
            var result = transform(numbers, x -> x + 100);
            System.out.println("result = " + Arrays.toString(result));
        }
    }

    int[] transform(int[] arr, TransformHandler<Integer> f) {
        var result = new int[arr.length];
        for (var k = 0; k < arr.length; k++) {
            result[k] = f.compute(arr[k]);
        }
        return result;
    }

}
