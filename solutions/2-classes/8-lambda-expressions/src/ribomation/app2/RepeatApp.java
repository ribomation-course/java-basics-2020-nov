package ribomation.app2;

import java.util.ArrayList;
import java.util.function.Consumer;

public class RepeatApp {
    public static void main(String[] args) {
        var app = new RepeatApp();
        app.run();
    }

    void run() {
        repeat(5, x -> System.out.printf("%d) tjabba habba%n", x));

        var lst = new ArrayList<String>();
        repeat(7, n -> lst.add("anna-" + n));
        System.out.println("lst = " + lst);
    }

    void repeat(int N, Consumer<Integer> expr) {
        for (var k = 1; k <= N; ++k) expr.accept(k);
    }

}
