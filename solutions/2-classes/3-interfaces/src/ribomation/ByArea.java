package ribomation;

import java.util.Comparator;

public class ByArea implements Comparator<Shape> {
    @Override
    public int compare(Shape lhs, Shape rhs) {
       // return Double.compare(lhs.area(), rhs.area());
        var areaLeft = lhs.area();
        var areaRight = rhs.area();
        if (areaLeft < areaRight) return -1;
        if (areaLeft > areaRight) return +1;
        return 0;
    }
}
