package ribomation;

import java.util.Locale;

public abstract class Shape {
    public abstract double area();

    protected abstract String name();

    @Override
    public final String toString() {
        return String.format(Locale.ENGLISH, "%-16s area=%.2f", name(), area());
    }
}
