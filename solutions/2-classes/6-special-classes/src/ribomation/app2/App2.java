package ribomation.app2;

import java.util.Locale;

public class App2 {
    public static void main(String[] args) {
        var app = new App2();
        app.run(17, 9);
        app.run(10, 15);
        app.run(3, -5);
    }

    void run(int left, int right) {
        for (var op : Operators.values()) print(left, op, op.symbol, right);
    }

    void print(int left, Operators op, String symbol, int right) {
        System.out.printf(Locale.ENGLISH, "%d %s %d = %.2f%n",
                left, symbol, right, op.eval(left, right));
    }
}
