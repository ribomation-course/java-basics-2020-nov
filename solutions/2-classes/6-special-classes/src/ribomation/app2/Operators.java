package ribomation.app2;

public enum Operators {
    plus("+"), minus("-"), times("*"), slash("/");

    public final String symbol;

    Operators(String symbol) {
        this.symbol = symbol;
    }

    double eval(int left, int right) {
        switch (this) {
            case plus:
                return left + right;
            case minus:
                return left - right;
            case times:
                return left * right;
            case slash:
                return (double) left / right;
        }
        throw new RuntimeException("unknown operator: " + this);
    }
}
