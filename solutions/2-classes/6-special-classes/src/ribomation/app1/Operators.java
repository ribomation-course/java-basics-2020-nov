package ribomation.app1;

public enum Operators {
    plus("+"),
    minus("-"),
    times("*"),
    slash("/"),
    modulus("%"),
    ;

    private final String symbol;
    Operators(String symbol) {
        this.symbol = symbol;
    }

    double eval(long lhs, long rhs) {
        switch (this) {
            case plus: return lhs + rhs;
            case minus: return lhs - rhs;
            case times: return lhs * rhs;
            case slash: return (double)lhs / rhs;
            case modulus: return lhs % rhs;
        }
        throw new IllegalArgumentException("invalid operator: " + this);
    }

    @Override
    public String toString() { return symbol; }
}
