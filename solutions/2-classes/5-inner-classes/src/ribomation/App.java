package ribomation;

import java.util.Comparator;
import java.util.Random;

import static java.util.Arrays.sort;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run(args.length == 0 ? 10 : Integer.parseInt(args[0]));
    }

    Random r = new Random();

    void run(int n) {
        Shape[] shapes = create(n);

        System.out.println("-- unsorted --");
        for (var s : shapes) System.out.println(s);

        System.out.println("-- sorted asc --");
        sort(shapes, new Comparator<Shape>() {
            @Override
            public int compare(Shape lhs, Shape rhs) {
                var areaLeft = lhs.area();
                var areaRight = rhs.area();
                if (areaLeft < areaRight) return -1;
                if (areaLeft > areaRight) return +1;
                return 0;
            }
        });
        for (var s : shapes) System.out.println(s);

        System.out.println("-- sorted desc --");
        sort(shapes, new Comparator<Shape>() {
            @Override
            public int compare(Shape left, Shape right) {
                var areaLeft = left.area();
                var areaRight = right.area();
                if (areaLeft < areaRight) return -1;
                if (areaLeft > areaRight) return +1;
                return 0;
            }
        }.reversed());
        for (var s : shapes) System.out.println(s);
    }

    Shape[] create(int n) {
        var shapes = new Shape[n];
        for (var k = 0; k < n; ++k) shapes[k] = create();
        return shapes;
    }

    Shape create() {
        switch (r.nextInt(3)) {
            case 0:
                return new Rect(val(), val());
            case 1:
                return new Triangle(val(), val());
            case 2:
                return new Circle(val());
        }
        throw new IllegalArgumentException("unexpected");
    }

    int val() {
        return 1 + r.nextInt(20);
    }
}
