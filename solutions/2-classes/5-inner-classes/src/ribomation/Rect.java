package ribomation;

public class Rect extends Shape {
    private final int w;
    private final int h;

    public Rect(int w, int h) {
        this.w = w;
        this.h = h;
    }

    @Override
    public double area() {
        return w * h;
    }

    @Override
    protected String name() {
        return String.format("Rect(%d, %d)", w, h);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rect rect = (Rect) o;

        if (w != rect.w) return false;
        return h == rect.h;
    }

    @Override
    public int hashCode() {
        int result = w;
        result = 31 * result + h;
        return result;
    }
}
