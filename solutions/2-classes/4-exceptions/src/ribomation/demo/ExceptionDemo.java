package ribomation.demo;

import java.io.IOException;

public class ExceptionDemo {
    public static void main(String[] args) {
        System.out.println("[main] enter");
        var app = new ExceptionDemo();
//        app.run1();
        app.run2();
//        app.run3();
        System.out.println("[main] exit");
    }

    void run1() {
        System.out.println("[run1] enter");
        fun1();
        System.out.println("[run1] exit");
    }

    void run2() {
        System.out.println("[run2] enter");
        try {
            fun1();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            System.out.println("[run2] done");
        }
        System.out.println("[run2] exit");
    }

    void run3() {
        System.out.println("[run3] enter");
        try {
            fun1();
        } catch (Exception e) {
            System.out.printf("Failure: %s%n", e.getMessage());
            var location = e.getStackTrace()[0];
            System.out.printf("in file \"%s\" at line %d",
                    location.getFileName(), location.getLineNumber()
            );
            System.out.printf(" which is class \"%s\" within method %s() %n",
                    location.getClassName(), location.getMethodName()
            );
        } finally {
            System.out.println("[run3] done");
        }
        System.out.println("[run3] exit");
    }

    void fun1()  {
        System.out.println("[fun1] enter");
        fun2();
        System.out.println("[fun1] exit");
    }

    void fun2()  {
        System.out.println("[fun2] enter");
        try {
            fun3();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("[fun2] exit");
    }

    void fun3() throws IOException {
        System.out.println("[fun3] enter");

//        Object obj = null;
//        System.out.println(obj.toString());

        throw new IOException("tjabba");

      //  System.out.println("[fun3] exit");
    }
}
