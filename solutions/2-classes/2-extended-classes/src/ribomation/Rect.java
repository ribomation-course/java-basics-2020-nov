package ribomation;

public class Rect extends Shape {
    private final int w;
    private final int h;

    public Rect(int w, int h) {
        this.w = w;
        this.h = h;
    }

    @Override
    public double area() {
        return w * h;
    }

    @Override
    public String toString() {
        return String.format("Rect(%d, %d) -> %.1f",
                this.w, this.h, this.area());
    }
}
