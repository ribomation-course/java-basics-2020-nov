package ribomation;

public class Triangle extends Shape {
    private final int b;
    private final int h;

    public Triangle(int b, int h) {
        this.b = b;
        this.h = h;
    }

    @Override
    public double area() {
        return b * h / 2.0;
    }

    @Override
    public String toString() {
        return String.format("Triangle(%d, %d) -> %.1f", b, h, area());
    }
}
