package ribomation;

import java.util.Random;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    Random r = new Random();

    void run() {
        Shape[] shapes = {
                new Rect(val(), val()),
                new Circle(val()),
                new Rect(val(), val()),
                new Triangle(val(), val()),
                new Circle(val()),
                new Triangle(val(), val()),
        };
        for (var s : shapes) System.out.println(s);
    }

    int val() {
        return 1 + r.nextInt(10);
    }
}
