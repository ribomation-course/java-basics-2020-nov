package ribomation;

import java.util.StringJoiner;

public class Person {
    private String name;
    private Car myCar;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Person{%s, %s}", name, myCar);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car getMyCar() {
        return myCar;
    }

    public void setMyCar(Car myCar) {
        this.myCar = myCar;
    }
}
