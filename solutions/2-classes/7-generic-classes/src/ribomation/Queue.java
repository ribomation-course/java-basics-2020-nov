package ribomation;

import java.util.Arrays;

public class Queue<ElemType> {
    private ElemType[] storage;
    private int size = 0;
    private int putIdx = 0;
    private int getIdx = 0;

    public Queue(int capacity) {
        storage = (ElemType[]) new Object[capacity];
    }

    public boolean full() { return size == storage.length; }
    public boolean empty() { return size == 0; }

    public void put(ElemType x) {
        storage[putIdx++] = x;
        if (putIdx >= storage.length) putIdx = 0;
        ++size;
    }

    public ElemType get() {
        var x = storage[getIdx++];
        if (getIdx >= storage.length) getIdx = 0;
        --size;
        return x;
    }

    void spy() {
        System.out.printf("q: %s%n", Arrays.toString(storage));
    }
}
