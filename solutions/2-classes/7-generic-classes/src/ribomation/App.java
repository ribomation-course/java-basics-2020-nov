package ribomation;

import java.util.Date;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
        app.run2();
        app.run3();
    }

    void run() {
        var q = new Queue<Integer>(5);
        for (var k = 1; !q.full(); ++k) q.put(k);
        q.spy();
        while (!q.empty()) System.out.println(q.get());
    }

    void run2() {
        var q = new Queue<String>(5);
        for (var k = 1; !q.full(); ++k) q.put("nisse-" + k);
        q.spy();
        while (!q.empty()) System.out.println(q.get());
    }

    void run3() {
        var q = new Queue<Person>(5);
        for (var k = 1; !q.full(); ++k) q.put(new Person("Nisse-"+k, 10*k));
        q.spy();
        while (!q.empty()) System.out.println(q.get());
    }

}
