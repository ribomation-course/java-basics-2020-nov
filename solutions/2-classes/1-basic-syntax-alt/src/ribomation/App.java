package ribomation;

public class App {
    private void run() {
        var person = new Person("Nisse Hult");
        System.out.println("person = " + person);

        var car = new Car("ABC123", "Volvo Amazon");
        System.out.println("car = " + car);

        person.setMyCar(car);
        System.out.println("person = " + person);
    }

    public static void main(String[] args) {
        var app = new App();
        app.run();
    }
}
