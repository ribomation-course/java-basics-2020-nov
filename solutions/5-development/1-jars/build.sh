#!/usr/bin/env bash
set -eux

JAVA_DIR=./src
OUT_DIR=./build
CLASS_DIR=$OUT_DIR/classes
JAR_DIR=$OUT_DIR/jars

rm -rf $OUT_DIR
mkdir -p $CLASS_DIR $JAR_DIR
javac -d $CLASS_DIR -cp $CLASS_DIR $(find $JAVA_DIR -name '*.java')
jar cfm $JAR_DIR/persons.jar ./meta/MANIFEST.MF -C $CLASS_DIR .
jar tvf $JAR_DIR/persons.jar
tree $OUT_DIR

