package ribomation;

import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    void run() {
        int k = 1;
        var persons = List.of(
                new Person("Nisse", k++ * 10),
                new Person("Anna", k++ * 10),
                new Person("Olle", k++ * 10),
                new Person("Berit", k++ * 10),
                new Person("Kurt", k++ * 10)
                );
        persons.forEach(System.out::println);

        var gson = new GsonBuilder().setPrettyPrinting().create();
        var result = gson.toJson(persons);
        System.out.println("result = " + result);
    }
}
