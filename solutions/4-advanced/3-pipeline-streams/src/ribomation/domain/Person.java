package ribomation.domain;

import java.util.StringJoiner;

public class Person {
    final String name;
    final boolean female;
    final int age;
    final int postCode;

    public Person(String name, String female, String age, String postCode) {
        this(
                name,
                female.equals("Female"),
                Integer.parseInt(age),
                Integer.parseInt(postCode)
        );
    }

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("female=" + female)
                .add("age=" + age)
                .add("postCode=" + postCode)
                .toString();
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }
}
