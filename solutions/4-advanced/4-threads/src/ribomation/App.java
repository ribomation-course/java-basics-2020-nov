package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {
    public static void main(String[] args) throws IOException {
        var app = new App();
        app.run(Path.of("./data/shakespeare.txt"));
    }

    void run(Path file) throws IOException {
        var payload = Files.readString(file);
        var numChunks = 100;
        var chunkSize = payload.length() / numChunks;

        var pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        var results = new ArrayList<Future<Map<String, Long>>>();
        for (var k = 0; k < numChunks; ++k) {
            var start = k * chunkSize;
            var end = (k + 1) * chunkSize;
            var chunk = payload.substring(start, end);
            var result = pool.submit(new Task(chunk));
            results.add(result);
        }

        var freqs = new HashMap<String, Long>();
        results.stream()
                .map(r -> {
                    try {
                        return r.get();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .forEach(r -> {
                    r.forEach((word, count) -> {
                        var prevCount = freqs.getOrDefault(word, 0L);
                        freqs.put(word, count + prevCount);
                    });
                });

        freqs.entrySet().stream()
                .sorted((lhs, rhs) -> (int) (rhs.getValue() - lhs.getValue()))
                .limit(100)
                .forEach(e -> System.out.printf("%s: %s%n", e.getKey(), e.getValue()));
        pool.shutdownNow();
    }

}
