package ribomation;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Task implements Callable<Map<String, Long>> {
    private final String chunk;

    public Task(String chunk) {
        this.chunk = chunk;
    }

    @Override
    public Map<String, Long> call() throws Exception {
        return Pattern.compile("[^a-zA-Z]+")
                .splitAsStream(chunk)
                .filter(word -> word.length() > 4)
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(
                        word -> word,
                        Collectors.counting()
                ));
    }
}
