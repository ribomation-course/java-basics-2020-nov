package ribomation;

import ribomation.domain.Person;
import ribomation.mappers.ResultSetMapper;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SelectApp {
    public static void main(String[] args) throws IOException, SQLException {
        var app = new SelectApp();
        app.run();
    }

    void run() throws IOException, SQLException {
        try (var con = openDB("/jdbc.properties")) {
            var persons = query(con);
            final int[] cnt = {1};
            persons.forEach(p -> System.out.printf("%d) %s%n", cnt[0]++, p));
        }
    }

    Connection openDB(String jdbcResource) throws SQLException, IOException {
        var props = new Properties();
        try (var is = getClass().getResourceAsStream(jdbcResource)) {
            props.load(is);
        }
        var host = props.getProperty("host");
        var port = Integer.parseInt(props.getProperty("port"));
        var db = props.getProperty("database");
        var usr = props.getProperty("user");
        var pwd = props.getProperty("password");

        var url = String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", host, port, db);

        return DriverManager.getConnection(url, usr, pwd);
    }

    List<Person> query(Connection con) throws SQLException {
        var persons = new ArrayList<Person>();
        var mapper = new ResultSetMapper();
        var sql = "select name,female,age,post_code from persons where female = true and age between 30 and 40 and post_code < 20000";

        try (var stmt = con.createStatement()) {
            try (var rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    var p = mapper.fromRS(rs);
                    persons.add(p);
                }
            }
        }

        return persons;
    }


}
