package ribomation.mappers;

import ribomation.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetMapper {
    public Person fromRS(ResultSet rs) throws SQLException {
        var name = rs.getString("name");
        var female = rs.getBoolean("female");
        var age = rs.getInt("age");
        var post_code = rs.getInt("post_code");
        return new Person(name, female, age, post_code);
    }
}


