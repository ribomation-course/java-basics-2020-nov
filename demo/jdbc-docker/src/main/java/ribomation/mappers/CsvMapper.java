package ribomation.mappers;

import ribomation.domain.Person;

public class CsvMapper {
    final String delim;

    public CsvMapper(String delim) {
        this.delim = delim;
    }

    public Person fromCsv(String csv) {
        //name;gender;age;post_code
        var fields = csv.split(delim);
        return new Person(fields[0], fields[1], fields[2], fields[3]);
    }

}
