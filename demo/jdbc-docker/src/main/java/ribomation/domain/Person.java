package ribomation.domain;

import java.util.StringJoiner;

public class Person {
    final String name;
    final boolean female;
    final int age;
    final int postCode;

    public Person(String name, String female, String age, String postCode) {
        this(
                name,
                female.equals("Female"),
                Integer.parseInt(age),
                Integer.parseInt(postCode)
        );
    }

    public Person(String name, boolean female, int age, int postCode) {
        this.name = name;
        this.female = female;
        this.age = age;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return String.format("Person[%s, %s, %d, %d]",
                getName(), isFemale() ? "female" : "male", getAge(), getPostCode());
    }

    public String getName() {
        return name;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public int getPostCode() {
        return postCode;
    }
}
